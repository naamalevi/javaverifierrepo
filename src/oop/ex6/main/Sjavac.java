package oop.ex6.main;

import java.io.IOException;

/**
 * A class contains a main function that can output whether the input file ia a legal sJava file or not.
 */
public final class Sjavac {

	//------------- static members --------------------//
	private final static String SJAVA_SUFFIX = ".sjava";
	private final static String NON_SJAVA_MSG = "Warning: Running on a non s-java file";
	private final static String NUM_OF_ARG_MSG = "Warning: illegal number of arguments";
	private final static int LEGAL_NUM_OF_ARG = 1;


	private final static int LEGAL_CODE = 0;
	private final static int ILLEGAL_CODE = 1;
	private final static int IO_ERROR_OCCURRED = 2;
	private final static int WRONG_USAGE = 2;


	/**
	 * A main function. Checks the validity of a given sJava file, and prints the result to the screen:
	 * 0 - For legal sJava file.
	 * 1 - For illegal sJava file.
	 * 2 - For IO errors, or incompatible arguments.
	 * The later two are followed by error description in the system.err.
	 * @param args Array of string represents the program arguments.
	 *                Expect to get one arg: a sJava file to validity check.
	 */
	public static void main(String[] args) {
		if (args.length != LEGAL_NUM_OF_ARG){
			System.out.println(WRONG_USAGE);
			System.err.println(NUM_OF_ARG_MSG);
			return;
		}
		if (!args[0].endsWith(SJAVA_SUFFIX)){
			System.out.println(WRONG_USAGE);
			System.err.println(NON_SJAVA_MSG);
			return;
		}
		Parser fileParser = new Parser();
		try{
			fileParser.analyzeFile(args[0]);
			System.out.println(LEGAL_CODE);
		}
		catch (IOException e){
 			System.out.println(IO_ERROR_OCCURRED);
			System.err.println(e.getMessage());
		}
		catch (CompileSjavaExceptions e){
			System.out.println(ILLEGAL_CODE);
			System.err.println(e.getMessage());
		}
	}

}