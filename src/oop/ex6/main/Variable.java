package oop.ex6.main;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * An abstract class represents a general variable in sJava file, contain its name, type and indicators if
 * was declared as final and if was initialized in its declaration scope.
 * Also contains collection of basic abstract methods which deals with specific variable.
 */
abstract class Variable {
	String name,type;
	boolean isInitialized, isFinal;

	/*
	 * Constructor, creates a new variable with the given attributes
	 * @param name The name of the variable.
	 * @param isFinal A boolean that indicates whether the variable was declared as final.
	 * @param type  The type of the variable.
	 * @param isInitialized A boolean that indicates whether the variable was initialized.
	 */
	Variable(String name, boolean isFinal, String type, boolean isInitialized)
	{
		this.name = name;
		this.isFinal = isFinal;
		this.type = type;
		this.isInitialized = isInitialized;
	}

	/*
	 * Checks whether a single assignment is valid.
	 * @param valueToAssign The value in the right side of the assignment
	 * @param allVar Array list of all the file's variables in the file's scope - hierarchy order.
	 * @throws CompileSjavaExceptions When the the assignment found as illegal.
	 */
	abstract void checkLegalAssign(String valueToAssign, ArrayList<HashMap<String, Variable>> allVar)
			throws CompileSjavaExceptions;

	/*
	 * Checks if the given parameter value matches the variable's type (directly, not as variable).
	 * @param paramValue A string representing the parameter value to check.
	 * @return True if it matches, False otherwise.
	 */
	abstract boolean matchToTypePattern(String paramValue);

	/*
	 * Checks whether a variable with the given type can be assign to the current variable, according to
	 * the type's table.
	 * @param paramType String that represents the type that should be checked.
	 * @return True if the type is Matching, False otherwise.
	 */
	abstract boolean isMatchingTypes(String paramType);
}
