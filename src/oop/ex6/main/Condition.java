package oop.ex6.main;

import java.io.BufferedReader;

/*
 * A class represents a condition scope (if or while) parser that can parse all the lines that associated
 * with the specific condition scope.
 */
class Condition extends Scope{

	private final static int CONDITION_PERTS = 2;

	@Override
	protected void parserScopeStart(BufferedReader reader, Parser parser) throws CompileSjavaExceptions {

		// remove first word
		parser.currentLine = parser.currentLine.split(Regex.CONDITION_DELIMITERS, CONDITION_PERTS)[1];
		parser.currentLine = parser.currentLine.replaceAll(Regex.CONDITION_EXTRA_CHARS, Regex.EMPTY_STR);
		String[] conditionsArray = parser.currentLine.split(Regex.BETWEEN_CONDITIONS);
		for (String condition : conditionsArray) {

			// call with exist var
			if (!condition.matches(Regex.BOOLEAN_FORMAT)) {
				if (condition.matches(Regex.VAR_NAME)) {
					Variable varToCompare = VarOperation.returnVariable(condition, parser.allVar);
					if (!(varToCompare.type.equals(Regex.BOOLEAN_CODE) ||
							varToCompare.type.equals(Regex.INT_CODE) ||
							varToCompare.type.equals(Regex.DOUBLE_CODE)) || !varToCompare.isInitialized) {
						throw new IfWhileException("Not a valid condition");
					}
				} else{
					throw new IfWhileException("Not a valid condition");
				}
			}
		}
	}
}
