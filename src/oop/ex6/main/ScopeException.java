package oop.ex6.main;

/*
 * This class represents all exceptions which occur in a single scope.
 */
class ScopeException extends CompileSjavaExceptions {
	private static final long serialVersionUID = 1L;

	/*
	 * @param msg The error msg.
	 */
	public ScopeException(String msg) {
		super(msg);
	}
}
