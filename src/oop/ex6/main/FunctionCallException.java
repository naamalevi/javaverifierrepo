package oop.ex6.main;

/*
 * This class represents all Function call exceptions.
 */
class FunctionCallException extends CompileSjavaExceptions {
	private static final long serialVersionUID = 1L;

	/*
	 * @param msg The error msg.
	 */
	FunctionCallException(String msg){
		super(msg);
	}
}
