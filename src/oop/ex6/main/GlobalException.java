package oop.ex6.main;

/*
 * This class represents all exceptions which occur in the Global scope.
 */
class GlobalException extends CompileSjavaExceptions {
	private static final long serialVersionUID = 1L;

	/*
	 * @param msg The error msg.
	 */
	GlobalException(String msg) {
		super(msg);
	}
}
