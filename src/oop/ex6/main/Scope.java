package oop.ex6.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;

/*
 * An abstract class represents a Scope parser that can parse all the lines that associated with the specific
 * scope.
 */
abstract class Scope {
	static int bracketsCtr = 0;
	static String lastLine = null;


	/*
	 * Deals with the scope parsing, excluding create its collection of local variables.
	 * @param reader BufferedReader uses to read the file to check.
	 * @param parser Parser that contains all the information that is currently needed for validity checks.
	 * @throws CompileSjavaExceptions When the scope was found as illegal.
	 * @throws IOException When IO error was occurred.
	 */
	void parseScope(BufferedReader reader, Parser parser) throws CompileSjavaExceptions,IOException{
		// create the HashMap of the local variables:
		parser.allVar.add(new HashMap<String, Variable>());
		scopeProcess(reader, parser, Regex.SCOPE_CLOSER);
		// remove the HashMap of the local variables:
		parser.allVar.remove(parser.allVar.size() - 1);
	}

	/*
	 * Parses all the scope and updates the parser's allVar accordingly.
	 * @param reader BufferedReader uses to read the file to check.
	 * @param parser Parser that contains all the information that is currently needed for validity checks.
	 * @param endOfScope A String that indicates that the scope in ended.
	 * @throws CompileSjavaExceptions When the scope was found as illegal.
	 * @throws IOException When IO error was occurred.
	 */
	void scopeProcess(BufferedReader reader, Parser parser, String endOfScope) throws
																	CompileSjavaExceptions,IOException {
		parserScopeStart(reader, parser);
		while (true){
			lastLine = parser.currentLine;
			parser.currentLine = reader.readLine();

			// the global scope parsing was finished
			if ((endOfScope == null) && (parser.currentLine == null)){
					break;
				}
			if (parser.currentLine == null){
					throw new ScopeException("illegal scope format");
				}
			// the scope parsing (exclude global) was finished
			if ((endOfScope != null) && (parser.currentLine.matches(Regex.SCOPE_CLOSER_FORM))){
					break;
			}

			 if (parser.currentLine.matches(Regex.WHITESPACES_LINE) ||
					 parser.currentLine.startsWith(Regex.COMMENT_STARTER)){
				continue;
			}
			parser.currentLine = parser.currentLine.replaceAll(Regex.AT_LEAST_ONE_WHITESPACE,
					Regex.ONE_SPACE).trim();
			parseScopeInnerLines(reader, parser);
			updateBracketsCtr(parser.currentLine); // relevant to the global scope
		}
		ifReturnLine();
	}

	/*
	 * Parses all the scope's inner lines and updates the parser's allVar accordingly.
	 * @param reader BufferedReader uses to read the file to check
	 * @param parser Parser that contains all the information that is currently needed for validity checks.
	 * @throws CompileSjavaExceptions When the an inner line was found as illegal.
	 * @throws IOException When IO error was occurred.
	 */
	void parseScopeInnerLines(BufferedReader reader, Parser parser)
												throws CompileSjavaExceptions, IOException {
		if (parser.currentLine.endsWith(Regex.SCOPE_CLOSER)) {
			ifReturnLine();
		}
		else if (parser.currentLine.matches(Regex.VAR_DEC)) {
			VarOperation.parseVarDec(parser.currentLine, parser.allVar,
					parser.currentLine.startsWith(Regex.FINAL_CODE));
		} else if (parser.currentLine.matches(Regex.VAR_ASSIGN)) {
			VarOperation.varAssign(parser.currentLine, parser.allVar, false);
		} else if (parser.currentLine.matches(Regex.FUNCTION_CALL)) {
			Function.parseFunctionCall(parser);
		} else if (parser.currentLine.matches(Regex.IF_WHILE_STARTER)) {
			Condition condition = new Condition();
			condition.parseScope(reader, parser);
		} else if (parser.currentLine.matches(Regex.RETURN_CODE)) {
		} else {
			throw new ScopeException("Not valid line in scope");
		}
	}

	/*
	 * Parses the Scope's first line, the function's declaration for function, and the condition's line
	 * for if/while scopes.
	 * @param reader BufferedReader uses to read the file to check
	 * @param parser Parser that contains all the information that is currently needed for validity checks.
	 * @throws CompileSjavaExceptions When this line was found as illegal.
	 */
	void parserScopeStart(BufferedReader reader, Parser parser) throws CompileSjavaExceptions{}

	/*
	 * checks whether it is valid that the current scope will be ended in this line.
	 * This function that is been override by Function class enforce scope from type function to end only
	 * if its last line was "return type".
	 * @throws CompileSjavaExceptions When the file was found as not legal sJava file, happens when the
	 * scope is Function and the last line wasn't return.
	 */
	void ifReturnLine() throws CompileSjavaExceptions{}

	/*
	 * Updates the counter of the brackets that indicates the scopes hierarchy, according to the given line.
	 * param currentLine A file's line that can affect the bracketsCtr.
	 */
	private void updateBracketsCtr(String currentLine) {
		if (currentLine.endsWith(Regex.SCOPE_STARTER)) {
			bracketsCtr++;
		}
		if (currentLine.endsWith(Regex.SCOPE_CLOSER)) {
			bracketsCtr--;
		}
	}
}

