package oop.ex6.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;

/*
 *  A class represents a Function scope parser that can parse all the lines that associated with the
 *  specific function.
 */
class Function extends Scope {

	private static final int FUNCTION_DEC_PARTS = 2;
	private static final int REGULAR_VAR_NAME_LOCATION = 1;
	private static final int FINAL_VAR_NAME_LOCATION = 2;

	//---------------------------------- static functions ----------------------------------------//

	/*
	 * A static function, runs over the file and parse all the function in the order they appear in the file.
	 * If a line was found as illegal - throws.
	 * @param input the file to check
	 * @param parser Parser that contains all the information that is currently needed for validity checks.
	 * @throws CompileSjavaExceptions When a function was found as illegal.
	 * @throws IOException When IO error was occurred.
	 */
	static void compileAllFunctions(File input, Parser parser)
			throws CompileSjavaExceptions, IOException {

		BufferedReader reader = new BufferedReader(new FileReader(input));
		lastLine = parser.currentLine;
		parser.currentLine = reader.readLine().replaceAll(Regex.AT_LEAST_ONE_SPACE, Regex.ONE_SPACE).trim();

		while (parser.currentLine != null) {
			Matcher functionM = Regex.FUNCTION_DEC.matcher(parser.currentLine);
			if (functionM.matches()) {
				Function currentFunc = new Function();
				currentFunc.parseScope(reader, parser);
			}
			lastLine = parser.currentLine;
			parser.currentLine = reader.readLine();
			parser.currentLine = (parser.currentLine != null) ? parser.currentLine.replaceAll
					(Regex.AT_LEAST_ONE_SPACE, Regex.ONE_SPACE).trim() : null;
		}
	}

	/*
	 * A static function, parse a line that use to declare a function.
	 * @param parser Parser that contains all the information that is currently needed for validity checks.
	 * @throws CompileSjavaExceptions When the function declaration was found as illegal.
	 */
	static void readFunctionDec(Parser parser) throws CompileSjavaExceptions {
		// remove first word.
		parser.currentLine = parser.currentLine.split(Regex.ONE_SPACE, FUNCTION_DEC_PARTS)[1];
		String[] splitLine = parser.currentLine.split(Regex.FUNCTION_DELIMITERS);
		String functionName = splitLine[0];
		ArrayList<Variable> parametersList = new ArrayList<Variable>();
		Matcher parameterM = Regex.PARAM_PATTERN.matcher(parser.currentLine);

		while (parameterM.find()) {
			String aParam = parser.currentLine.substring(parameterM.start(), parameterM.end());
			String[] splitParam = aParam.split(Regex.ONE_WHITESPACE);
			Variable newVar;
			String firstDecWord = splitParam[0];
			if (firstDecWord.equals(Regex.FINAL_CODE)) {
				newVar = VariablesFactory.createNewVar(splitParam[FINAL_VAR_NAME_LOCATION], true,
						splitParam[VarOperation.FINAL_VAR_TYPE_LOCATION], true);
			} else {
				newVar = VariablesFactory.createNewVar(splitParam[REGULAR_VAR_NAME_LOCATION], false,
						splitParam[VarOperation.REGULAR_VAR_TYPE_LOCATION], true);
			}
			if (VarOperation.varNameIsExist(parametersList, newVar)) {
				throw new FunctionDecException("Parameter name already exists in scope");
			}
			parametersList.add(newVar);
		}
		parser.allFunctionsDec.put(functionName, parametersList);

	}

	/*
	 * Parse a line that calls a function.
	 * parser Parser that contains all the information that is currently needed for validity checks.
	 * throws CompileSjavaExceptionsWhen the function call was found as illegal.
	 */
	static void parseFunctionCall(Parser parser) throws CompileSjavaExceptions {
		parser.currentLine = parser.currentLine.replaceAll(Regex.GENERIC_EXTRA_CHARS, Regex.EMPTY_STR);
		String[] splitLine = parser.currentLine.split(Regex.FUNCTION_DELIMITERS);
		String funcName = splitLine[0];

		if (parser.allFunctionsDec.containsKey(funcName)) {
			ArrayList<Variable> funcArgs = parser.allFunctionsDec.get(funcName);
			if ((splitLine.length - 1) != funcArgs.size()) {
				throw new FunctionCallException("illegal number of arguments");
			}
			for (int i = 0; i < funcArgs.size(); i++) {
				// call with exist var
				if (splitLine[i + 1].matches(Regex.VAR_NAME) && !splitLine[i + 1].equals(Regex.FALSE_CODE) &&
						(!splitLine[i + 1].equals(Regex.TRUE_CODE))) {
					Variable varToCompare = VarOperation.returnVariable(splitLine[i + 1], parser.allVar);
					VarOperation.checkExistingVarTypeMatch(funcArgs.get(i), varToCompare);
				}
				//call with a primitive
				else {
					VarOperation.checkPrimitiveTypeMatch(funcArgs.get(i), splitLine[i + 1]);
				}
			}
		} else {
			throw new FunctionCallException("No such function");
		}
	}

	//----------------------------------  methods ----------------------------------------//

	@Override
	void parserScopeStart(BufferedReader reader, Parser parser) {
		// remove first word.
		parser.currentLine = parser.currentLine.split(Regex.ONE_SPACE, FUNCTION_DEC_PARTS)[1];
		String[] splitLine = parser.currentLine.split(Regex.FUNCTION_DELIMITERS);
		String funcName = splitLine[0];
		for (Variable argument : parser.allFunctionsDec.get(funcName)) {
			if (!parser.allVar.get(parser.allVar.size() - 1).containsKey(argument.name)) {
				parser.allVar.get(parser.allVar.size() - 1).put(argument.name, argument);
			}
		}
	}

	@Override
	void ifReturnLine() throws CompileSjavaExceptions {
		if (!lastLine.matches(Regex.RETURN_CODE)) {
			throw new ScopeException("Illegal scope format");
		}
	}
}
