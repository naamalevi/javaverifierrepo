package oop.ex6.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;

/*
 * A class represents a GlobalScope that can parse all the global scope and update the given parser's allVar
 * accordingly.
 */
class GlobalScope extends Scope{

	@Override
	void parseScope(BufferedReader reader, Parser parser) throws CompileSjavaExceptions,IOException {
		// create the HashMap of the local variables:
		parser.allVar.add(new HashMap<String, Variable>());
		bracketsCtr = 0;
		scopeProcess(reader, parser, null);
	}

	@Override
	void parseScopeInnerLines(BufferedReader reader, Parser parser) throws CompileSjavaExceptions{
		if (bracketsCtr == 0) { // this line is in the global scope
			Matcher functionM = Regex.FUNCTION_DEC.matcher(parser.currentLine);
			Matcher varDecM = Regex.VAR_DEC_PATTERN.matcher(parser.currentLine);
			Matcher varAssignM = Regex.VAR_ASSIGN_PATTERN.matcher(parser.currentLine);
			if (functionM.matches()) {
				Function.readFunctionDec(parser);
			} else if (varDecM.matches()) {
				VarOperation.parseVarDec(parser.currentLine, parser.allVar,
						parser.currentLine.startsWith(Regex.FINAL_CODE));
			} else if (varAssignM.matches()){
				VarOperation.varAssign(parser.currentLine, parser.allVar, false);
			} else {
				throw new GlobalException("Not valid format");
			}
		}
	}
}
