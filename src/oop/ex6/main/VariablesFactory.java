package oop.ex6.main;

/*
 * A Class responsible for creating new variables.
 */
class VariablesFactory {

	/*
	 * Creates new variables according to to their matching type pattern.
	 * @param name The name of the variable.
	 * @param isFinal A boolean that indicates whether the variable was declared as final.
	 * @param type  The type of the variable.
	 * @param isInitialized A boolean that indicates whether the variable was initialized.
	 * @return The new Variables object.
	 * @throws CompileSjavaExceptions if the given type didn't match any type pattern.
	 */
	static Variable createNewVar(String name, boolean isFinal, String type, boolean isInitialized)
			throws CompileSjavaExceptions{
		switch (type){
			case Regex.INT_CODE:
				return new IntVar(name, isFinal, type, isInitialized);
			case Regex.DOUBLE_CODE:
				return new DoubleVar(name, isFinal, type, isInitialized);
			case Regex.STRING_CODE:
				return new StringVar(name, isFinal, type, isInitialized);
			case Regex.BOOLEAN_CODE:
				return new BooleanVar(name, isFinal, type, isInitialized);
			case Regex.CHAR_CODE:
				return new CharVar(name, isFinal, type, isInitialized);
			default:
				throw new VariableDecException("Unknown type");
		}
	}
}
