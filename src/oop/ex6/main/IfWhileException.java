package oop.ex6.main;

/*
 * This class represents all exceptions in if/while statements.
 */
class IfWhileException extends CompileSjavaExceptions {
	private static final long serialVersionUID = 1L;

	/*
	 * @param msg The error msg.
	 */
	IfWhileException(String msg) {
		super(msg);
	}
}
