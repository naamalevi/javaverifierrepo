package oop.ex6.main;

import java.util.regex.Pattern;

/*
 * An abstract class of static final members,
 * collection of regular expressions that are useful for sJava validity verifier.
 */
abstract class Regex {

	//---------------------------------------  Strings  ----------------------------------------//
	static final String SCOPE_CLOSER_FORM = "\\s*}";
	static final String SCOPE_CLOSER = "}";
	static final String SCOPE_STARTER = "{";
	static final String FINAL_CODE = "final";
	static final String EMPTY_STR = "";
	static final String FALSE_CODE = "false";
	static final String TRUE_CODE = "true";
	static final String INT_CODE = "int";
	static final String DOUBLE_CODE = "double";
	static final String BOOLEAN_CODE = "boolean";
	static final String CHAR_CODE = "char";
	static final String STRING_CODE = "String";
	static final String COMMENT_STARTER = "//";
	static final String WHITESPACES_LINE = "\\s*";
	static final String INT_FORMAT = "-?\\d+";
	static final String DOUBLE_FORMAT = "-?((\\d+)|((\\d)+\\.(\\d)+))";
	static final String STRING_FORMAT = "\"[^\"]*\"";
	static final String BOOLEAN_FORMAT = "-?(\\d+)|-?((\\d+)|(\\d)+\\.(\\d)+)|true|false";
	static final String CHAR_FORMAT = "'.'";
	static final char ASSIGN_CODE = '=';
	private static final String OPTIONAL_FINAL = "(final )?";
	private static final String TYPE_OPTIONS = "(int|double|char|String|boolean)";
	private static final String CONDITION_OPTIONS = "(if|while)";
	private static final String NUMERICAL_TYPES_FORM = "((-)?(\\w)+(\\.(\\w)+)?)";
	private static final String STRING_FORM = "(\").*";
	private static final String CHAR_FORM = "(').";

	static final String AT_LEAST_ONE_WHITESPACE = "\\s+";
	static final String AT_LEAST_ONE_SPACE = " +";
	static final String ONE_SPACE = " ";
	static final String ONE_WHITESPACE = "\\s";
	static final String FUNCTION_DELIMITERS = "[(,)]";
	static final String CONDITION_DELIMITERS = "[( )]";
	static final String GENERIC_EXTRA_CHARS = "[ ;]";
	static final String CONDITION_EXTRA_CHARS = "[(){ ]";
	static final String BETWEEN_CONDITIONS = "(&&)|(\\|{2})";
	static final String VAR_NAME = "(_\\w|[A-Za-z])\\w*";
	static final String SINGLE_VAR_DEC_DELIMITERS = "[ =]";
	static final String RETURN_CODE = "\\s*return( *);";
	private static final String FUNCTION_NAME = "[A-Za-z]\\w*";
	private static final String GENERIC_ARGUMENT = "([\"'])?[\\w\\.]*([\"'])?";
	private static final String GENERIC_CONDITION = "[\\w\\.-]*";

	static final String VAR_DEC = OPTIONAL_FINAL + " *" + TYPE_OPTIONS + " " + Regex.VAR_NAME +
			" *(= *((" + STRING_FORM + "\\7)|" + NUMERICAL_TYPES_FORM + "|(" + CHAR_FORM + "\\14)))?( *, *" +
			Regex.VAR_NAME + " *(= *((" + STRING_FORM + "\\20)|" + NUMERICAL_TYPES_FORM + "|(" + CHAR_FORM +
			"\\27)))?)* ?;";
	static final String VAR_ASSIGN = VAR_NAME +" *= *((" + STRING_FORM + "\\4)|" +
			NUMERICAL_TYPES_FORM + "" + "|(" + CHAR_FORM + "\\11));";
	static final String FUNCTION_CALL = FUNCTION_NAME + "( )?\\(( )?(" + OPTIONAL_FINAL +
			GENERIC_ARGUMENT + "( )?, *)*(" + OPTIONAL_FINAL + GENERIC_ARGUMENT + ")?( )?\\) *;";
	static final String IF_WHILE_STARTER = CONDITION_OPTIONS + " ?\\( *" + GENERIC_CONDITION +
			"( *(" + BETWEEN_CONDITIONS + ") *" + GENERIC_CONDITION + ")* *\\) ?\\{";


	//---------------------------------------  Patterns ----------------------------------------//

	static final Pattern PARAM_PATTERN = Pattern.compile(OPTIONAL_FINAL + TYPE_OPTIONS + " +" + VAR_NAME);
	static final Pattern VAR_ASSIGN_PATTERN = Pattern.compile(VAR_NAME + " *= *((" + STRING_FORM + "\\4)|" +
			NUMERICAL_TYPES_FORM + "|(" + CHAR_FORM + "\\11));");
	static final Pattern SINGLE_VAR_DEC = Pattern.compile(VAR_NAME + " *(= *((" + STRING_FORM + "\\5)|" +
			NUMERICAL_TYPES_FORM + "|(" + CHAR_FORM + "\\12)))?");
	static final Pattern FUNCTION_DEC = Pattern.compile("void " + FUNCTION_NAME + " *\\(( )?((" +
			OPTIONAL_FINAL + TYPE_OPTIONS + " " +VAR_NAME + "( )?, )*(" + OPTIONAL_FINAL + TYPE_OPTIONS +
			" " + VAR_NAME + ")?)? *\\) *\\{");
	static final Pattern VAR_DEC_PATTERN = Pattern.compile(OPTIONAL_FINAL + " *" + TYPE_OPTIONS + " " +
			VAR_NAME + " *(= *((" + STRING_FORM + "\\7)|" + NUMERICAL_TYPES_FORM + "|(" + CHAR_FORM +
			"\\14)))?( *, *" + VAR_NAME + " *(= *((" + STRING_FORM + "\\20)|" + NUMERICAL_TYPES_FORM +
			"|(" + CHAR_FORM + "\\27)))?)* ?;");


}
