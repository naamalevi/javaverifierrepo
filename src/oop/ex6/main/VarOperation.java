package oop.ex6.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;

/*
 * An abstract class that contains collection of static function that manipulates the file's variables and
 * deals with their declarations and assignments.
 */
abstract class VarOperation {

	static final int REGULAR_VAR_TYPE_LOCATION = 0;
	static final int FINAL_VAR_TYPE_LOCATION = 1;
	private static final int REGULAR_PARTS_OF_LINE = 2;
	private static final int FINAL_PARTS_OF_LINE = 3;

	/*
	 * Parses a declaration of variable, and updates the given allVar accordingly.
	 * currentLine The line of the variables' declaration.
	 * @param allVar Array list of all the file's variables in the file's scope - hierarchy order.
	 * @param isFinal A boolean indicates whether the declaration is of final var (true) or not (false).
	 * @throws CompileSjavaExceptions When the declaration was found as illegal
	 */
	static void parseVarDec(String currentLine, ArrayList<HashMap<String, Variable>> allVar, boolean isFinal)
			throws CompileSjavaExceptions {

		String[] splitLine = currentLine.split(Regex.ONE_SPACE);
		String type = (isFinal) ? splitLine[FINAL_VAR_TYPE_LOCATION] : splitLine[REGULAR_VAR_TYPE_LOCATION];
		int partsOfLine = (isFinal)? FINAL_PARTS_OF_LINE : REGULAR_PARTS_OF_LINE;
		// remove the line's prefix, type and final (if exists)
		currentLine = currentLine.split(Regex.ONE_SPACE, partsOfLine)[partsOfLine - 1];
		Matcher singleVarDecM = Regex.SINGLE_VAR_DEC.matcher(currentLine);

		while (singleVarDecM.find()) {
			String aVar = currentLine.substring(singleVarDecM.start(), singleVarDecM.end());
			String[] splitVar = aVar.split(Regex.SINGLE_VAR_DEC_DELIMITERS);
			String varName = splitVar[0];
			Variable newVar = VariablesFactory.createNewVar(varName, isFinal, type, false);
			if (!allVar.get(allVar.size()-1).containsKey(splitVar[0])) {
				allVar.get(allVar.size()-1).put(splitVar[0], newVar);
			} else {
				throw new VariableDecException("Variable name is already in use in scope");
			}
			// if also assignment
			if (aVar.indexOf(Regex.ASSIGN_CODE) >= 0) {
				varAssign(aVar, allVar, true);
			}
			else{
				// final with no assignment
				if (isFinal){
					throw new VariableDecException("illegal format for final variable");
				}
			}
		}
	}

	/*
	 * Parses an assignment of a single variable, and marks as initialized when the declaration and the
	 * assignment are in the same scope.
	 * @param aVar The part of a line that contains the assignment.
	 * @param allVar Array list of all the file's variables in the file's scope - hierarchy order.
	 * @param allowFinal A boolean indicates whether to allow assignment into a final variable
	 * (when the declaration and the assignment are in the same line) (true) or not (false).
	 * @throws CompileSjavaExceptions When the assignment eas found as illegal.
	 */
	static void varAssign(String aVar, ArrayList<HashMap<String, Variable>> allVar, boolean allowFinal)
			throws CompileSjavaExceptions {

		aVar = aVar.replaceAll(Regex.GENERIC_EXTRA_CHARS, Regex.EMPTY_STR);
		int assignCode = aVar.indexOf(Regex.ASSIGN_CODE);
		String valueToAssign = (aVar.substring(assignCode + 1));
		String variableNameToAssign = (aVar.substring(0, assignCode));
		Variable varToAssign = returnVariable(variableNameToAssign, allVar);
		if (!allowFinal && varToAssign.isFinal) {
			throw new VarAssignException("Illegal assignment");
		}
		varToAssign.checkLegalAssign(valueToAssign, allVar);

		// mark as initialized if the assign is in the declaration scope
		if (allVar.get(allVar.size()-1).containsKey(varToAssign.name)) {
			varToAssign.isInitialized = true;
		}
	}

	/*
	 * Checks if the variable was declared in a previous scope, if so, returns the information about it
	 * (the instance),throws otherwise.
	 * @param variableNameToAssign A string that represents the name of the desired variable.
	 * @param allVar Array list of all the file's variables in the file's scope - hierarchy order.
	 * @return The inner most variable that named as the given name.
	 * @throws CompileSjavaExceptions When no such argument.
	 */
	static Variable returnVariable(String variableNameToAssign, ArrayList<HashMap<String, Variable>> allVar)
			throws CompileSjavaExceptions {
		for (int i = allVar.size() - 1; i>=0; i--){
			if (allVar.get(i).containsKey(variableNameToAssign)){
				return allVar.get(i).get(variableNameToAssign);
			}
		}
		throw new VarAssignException("Variable does not exist");
	}

	/*
	 * Checks if an argument of function is valid for it's specific location, if the second variable can be
	 * used as the first one.
	 * @originalVar The variable that the function expect to get.
	 * @paramVar The variable that actually appears in the function call.
	 * @throws CompileSjavaExceptions CompileSjavaExceptions When a function argument was found as illegal.
	 */
	static void checkExistingVarTypeMatch(Variable originalVar, Variable paramVar) throws
			CompileSjavaExceptions {
		if (!originalVar.isMatchingTypes(paramVar.type) ||
				(originalVar.isFinal && !paramVar.isFinal) || !paramVar.isInitialized) {
			throw new FunctionCallException("Illegal arguments to function");
		}
	}

	/*
	 * Checks if a primitive value is valid for the type of the variable that should get it.
	 * @param originalVar A variable to assign to.
	 * @param paramValue A string represents the value that should assign.
	 * @throws CompileSjavaExceptions CompileSjavaExceptions When a function argument was found as illegal.
	 */
	static void checkPrimitiveTypeMatch(Variable originalVar, String paramValue) throws
			CompileSjavaExceptions {
		if (!originalVar.matchToTypePattern(paramValue)) {
			throw new FunctionCallException("Illegal arguments to function");
		}
	}

	/*
	 * Checks if a given array contains a variable with tha same name as the given variable.
	 * @param parametersList Array list of variable to search in.
	 * @param newVar A variable that contains the name to search.
	 * @return true if parametersList contains a variable named as newVar, false otherwise.
	 */
	static boolean varNameIsExist(ArrayList<Variable> parametersList, Variable newVar) {
		for (Variable variable : parametersList) {
			if (variable.name.equals(newVar.name)) {
				return true;
			}
		}
		return false;
	}

	/*
	 * Checks that a variable was initialized in previous scope, and that in the inner most of them
	 * it holds a valid type, a type that can be assigned to the given type.
	 * type - String that represents the type that the variable expect to get.
	 * valueToAssign - The value in the right side of the assignment
	 * allVar - Array list of all the file's variables in the file's scope - hierarchy order.
	 * return - true if so, false otherwise.
	 */
	static boolean initializedInPreviousScope(Variable variable, String valueToAssign,
													  ArrayList<HashMap<String, Variable>> allVar){
		for (int i = allVar.size() - 1; i>=0; i--){
			if (allVar.get(i).containsKey(valueToAssign)){
				Variable theVariable = allVar.get(i).get(valueToAssign);
				if (variable.isMatchingTypes(theVariable.type) && (theVariable.isInitialized)){
					return true;
				}
			}
		}
		return false;
	}
}
