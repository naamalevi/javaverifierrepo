package oop.ex6.main;

/*
 * This class represents all variable assignments exceptions.
 */
class VarAssignException extends CompileSjavaExceptions {
	private static final long serialVersionUID = 1L;

	/*
	 * @param msg The error msg.
	 */
	public VarAssignException(String msg) {
		super(msg);
	}
}
