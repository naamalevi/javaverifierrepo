package oop.ex6.main;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * This class represents a string variable.
 */
class StringVar extends Variable {
	/*
	 * Constructor, creates a new String variable with the given attributes
	 * @param name The name of the variable.
	 * @param isFinal A boolean that indicates whether the variable was declared as final.
	 * @param type  The type of the variable.
	 * @param isInitialized A boolean that indicates whether the variable was initialized.
	 */
	StringVar(String name, boolean isFinal, String type, boolean isInitialized) {
		super(name, isFinal, type, isInitialized);
	}

	@Override
	boolean matchToTypePattern(String paramValue) {
		return paramValue.matches(Regex.STRING_FORMAT);
	}

	@Override
	void checkLegalAssign(String valueToAssign, ArrayList<HashMap<String, Variable>> allVar)
			throws CompileSjavaExceptions {
		if (!(VarOperation.initializedInPreviousScope(this, valueToAssign, allVar) ||
				valueToAssign.matches(Regex.STRING_FORMAT))){
			throw new VarAssignException("Illegal assignment");
		}
	}

	@Override
	boolean isMatchingTypes(String paramType) {
		return paramType.equals(Regex.STRING_CODE);
	}
}
