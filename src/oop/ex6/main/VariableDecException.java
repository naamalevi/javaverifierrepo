package oop.ex6.main;


/*
 * This class represents all exceptions in variable decelerations.
 */
class VariableDecException extends CompileSjavaExceptions {
	private static final long serialVersionUID = 1L;

	/*
	 * @param msg The error msg.
	 */
	public VariableDecException(String msg) {
		super(msg);
	}
}
