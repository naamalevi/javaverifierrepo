package oop.ex6.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/*
 * A class represents a Parser that holds all the information that is currently needed for validity checks.
 * Can go over a given sJava file and check it's validity.
 */
class Parser {

	// Array list of all the file's variables, according to the scope where they were declared.
	// The array holds the scopes in ascending order, meaning that the first member is for the global's scope
	// variable, the second for the associated function's variables, and so on.
	ArrayList<HashMap<String,  Variable>> allVar;

	// Contains the information about all the file's functions, accessing by the function name.
	HashMap<String, ArrayList<Variable>> allFunctionsDec;

	// Contains the line that is currently checked.
	String currentLine = null;


	/*
	 * Constructor, creates new Parser initializes with empty structures of functions and variables.
	 */
	Parser() {
		allVar = new ArrayList<>();
		allFunctionsDec = new HashMap<>();
	}

	/*
	 * Analyses the given file according to the sJava rules.
	 * @param inputPath A string represents the file-to-check's path.
	 * @throws CompileSjavaExceptions When the file was found as illegal sJava file.
	 * @throws IOException When IO error was occurred.
	 */
	void analyzeFile(String inputPath) throws CompileSjavaExceptions, IOException {
		File input = new File(inputPath);
		BufferedReader reader = new BufferedReader(new FileReader(input));
		GlobalScope globalParser = new GlobalScope();
		globalParser.parseScope(reader, this);
		Function.compileAllFunctions(input, this);
	}
}