package oop.ex6.main;

/*
 * This class represents all Function decelerations exceptions.
 */
class FunctionDecException extends CompileSjavaExceptions {
	private static final long serialVersionUID = 1L;

	/*
	 * @param msg The error msg.
	 */
	FunctionDecException(String msg) {
		super(msg);
	}
}
