package oop.ex6.main;

/*
 * This class represents all possible s-java compilation errors.
 */
class CompileSjavaExceptions extends Exception{
	private static final long serialVersionUID = 1L;

	/*
	 * @param msg The error msg.
	 */
	CompileSjavaExceptions(String msg){
		super(msg);
	}
}
